<?php

namespace PLC\PrintOuts\Plugin\Source;

use PLC\PrintOuts\Model\Config;

class PlaceForUse
{
    /** @var Config  */
    protected $config;

    public function __construct(
        Config $config
    ) {
        $this->config = $config;
    }

    /**
     * @param $subject
     * @param $result
     * @return mixed
     */
    public function afterToOptionArray($subject, $result)
    {
        if (!$this->config->isEnabled()) {
            return $result;
        }
        $result = \array_merge($result, $this->config->getTemplateTypes());
        return $result;
    }
}