<?php

namespace PLC\PrintOuts\Block\Adminhtml\System\Config;

use Magento\Config\Block\System\Config\Form\Field\FieldArray\AbstractFieldArray;

class TemplateType extends AbstractFieldArray
{
    /**
     * Prepare to render
     *
     * @return void
     */
    protected function _prepareToRender()
    {
        $this->addColumn('title', [
            'label' => __('Template Type'),
            'style' => 'width:300px',
        ]);
        $this->_addAfter = false;
        $this->_addButtonLabel = __('Add');
    }
}