<?php

namespace PLC\PrintOuts\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\DB\Ddl\Table;
use Amasty\PDFCustom\Model\ResourceModel\Template as TemplateResource;

/**
 * Class InstallSchema
 */
class InstallSchema implements InstallSchemaInterface
{
    /**
     * {@inheritdoc}
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     * @throws \Zend_Db_Exception
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;
        $installer->startSetup();

        $installer->getConnection()->changeColumn(
            $installer->getTable(TemplateResource::MAIN_TABLE),
            'place_for_use',
            'place_for_use',
            [
                'type'     => Table::TYPE_INTEGER,
                'length'   => 10,
                'unsigned' => true,
                'nullable' => false,
                'default'  => 0,
                'comment'  => 'Template Type'
            ]
        );
        $installer->endSetup();
    }
}