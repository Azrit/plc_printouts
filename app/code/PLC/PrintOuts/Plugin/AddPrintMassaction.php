<?php

namespace PLC\PrintOuts\Plugin;

use Magento\Framework\UrlInterface;
use PLC\PrintOuts\Model\Config;
use Magento\Ui\Component\ActionFactory;

class AddPrintMassaction
{
    /**
     * @var Config
     */
    protected $config;

    /**
     * Url Builder
     *
     * @var \Magento\Framework\UrlInterface
     */
    protected $urlBuilder;

    /**
     * @var ActionFactory
     */
    protected $actionFactory;

    /**
     * AddPrintMassaction constructor.
     * @param Config $config
     * @param UrlInterface $urlBuilder
     * @param ActionFactory $actionFactory
     */
    public function __construct(
        Config $config,
        UrlInterface $urlBuilder,
        ActionFactory $actionFactory
    ) {
        $this->config = $config;
        $this->urlBuilder = $urlBuilder;
        $this->actionFactory = $actionFactory;
    }

    /**
     * @param \Magento\Ui\Component\MassAction $subject
     */
    public function beforePrepare($subject)
    {
        if (
            $subject->getContext()->getNamespace() === 'sales_order_grid' &&
            $this->config->isEnabled()
        ) {
            $types = $this->config->getTemplateTypes();
            foreach ($types as $type) {
                /** @var \Magento\Ui\Component\Action $action */
                $action = $this->actionFactory->create();
                $action->setData('config', [
                    'component' => 'uiComponent',
                    'type'      => 'print_' . $type['value'],
                    'label'     => $type['label'],
                    'url'       => $this->getPrintUrl($type['value'])
                ]);
                $subject->addComponent('print_' . $type['value'], $action);
            }
        }
    }

    /**
     * @param $type
     * @return string
     */
    protected function getPrintUrl($type)
    {
        return $this->urlBuilder->getUrl('amasty_pdf/order/pdfOrders', ['type' => $type]);
    }
}