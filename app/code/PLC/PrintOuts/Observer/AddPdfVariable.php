<?php

namespace PLC\PrintOuts\Observer;

use Magento\Framework\Event\Observer as EventObserver;
use Magento\Framework\Event\ObserverInterface;
use PLC\PrintOuts\Model\Config;
use Psr\Log\LoggerInterface;

class AddPdfVariable implements ObserverInterface
{
    /** @var Config  */
    protected $config;

    /** @var LoggerInterface  */
    protected $logger;

    public function __construct(
        Config $config,
        LoggerInterface $logger
    ) {
        $this->config = $config;
        $this->logger = $logger;
    }

    /**
     * @param EventObserver $observer
     * @return $this
     */
    public function execute(EventObserver $observer)
    {
        if (!$this->config->isEnabled()) {
            return $this;
        }
        try {
            $transportObject = $observer->getEvent()->getTransportObject();
            //@todo add variables invoice|shipment|creditmemo if needed
        } catch (\Exception $e) {
            $this->logger->critical($e);
        }
        return $this;
    }
}