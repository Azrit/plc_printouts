<?php

namespace PLC\PrintOuts\Model;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Store\Model\ScopeInterface;
use Magento\Config\Model\ResourceModel\Config as ResourceConfig;
use Magento\Framework\Serialize\Serializer\Json;

class Config
{
    const GENERAL_ENABLED       = 'plc_printout/general/is_enabled';
    const GENERAL_TEMPLATE_TYPE = 'plc_printout/general/template_type';

    /** @var Json  */
    protected $serializer;

    /** @var ScopeConfigInterface  */
    protected $scopeConfig;

    public function __construct(
        ScopeConfigInterface $scopeConfig,
        Json $serializer
    ) {
        $this->scopeConfig = $scopeConfig;
        $this->serializer = $serializer;
    }

    /**
     * @param null $store
     * @return bool
     */
    public function isEnabled($store = null)
    {
        if (null === $store) {
            return (bool)$this->scopeConfig->getValue(
                self::GENERAL_ENABLED,
                'default'
            );
        }
        return (bool)$this->scopeConfig->getValue(
            self::GENERAL_ENABLED,
            ScopeInterface::SCOPE_STORE,
            $store
        );
    }

    /**
     * @param null $store
     * @return array|bool|float|int|mixed|null|string
     */
    public function getTemplateTypes($store = null)
    {
        $types = $this->scopeConfig->getValue(
            self::GENERAL_TEMPLATE_TYPE,
            ScopeInterface::SCOPE_STORE,
            $store
        );
        $types = $this->serializer->unserialize($types);
        $_result = [];
        foreach ($types as $type) {
            array_push($_result, [
                'value' => \crc32($type['title']),
                'label' => __($type['title'])
            ]);
        }
        return $_result;
    }
}