<?php
namespace PLC\PrintOuts\Block\Adminhtml\Order\Tab;

use Magento\Backend\Block\Template\Context;
use PLC\PrintOuts\Model\Config;
use Magento\Backend\Block\Template;
use Magento\Framework\Registry;

class Type extends Template
{
    /** @var Config  */
    protected $config;

    protected $coreRegistry;

    public function __construct(
        Context $context,
        Config $config,
        Registry $coreRegistry,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->config = $config;
        $this->coreRegistry = $coreRegistry;
    }

    /**
     * @return string
     */
    public function getPrintUrl()
    {
        return $this->getUrl('amasty_pdf/order/printAction', ['order_id' => $this->getOrderId()]);
    }

    /**
     * @return array|bool|float|int|mixed|null|string
     */
    public function getList()
    {
        return $this->config->getTemplateTypes();
    }

    /**
     * @return mixed
     */
    protected function getOrderId()
    {
        return $this->coreRegistry->registry('current_order')->getId();
    }
}