<?php

namespace PLC\PrintOuts\Controller\Adminhtml\Template;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Email\Model\Template\Config;
use Amasty\PDFCustom\Model\TemplateFactory;

class DefaultTemplate extends Action
{
    const ADMIN_RESOURCE = 'Amasty_PDFCustom::template';

    /**
     * @var \Magento\Email\Model\Template\Config
     */
    protected $emailConfig;

    /**
     * @var \Amasty\PDFCustom\Model\TemplateFactory
     */
    protected $templateFactory;

    public function __construct(
        Context $context,
        Config $emailConfig,
        TemplateFactory $templateFactory
    ) {
        $this->emailConfig = $emailConfig;
        $this->templateFactory = $templateFactory;
        parent::__construct($context);
    }

    /**
     * Set template data to retrieve it in template info form
     *
     * @return void
     * @throws \RuntimeException
     */
    public function execute()
    {
        $template = $this->templateFactory->create();
        $templateId = $this->getRequest()->getParam('code');
        try {
            $template->load($templateId);
        } catch (\Exception $e) {

        }

        if (!$template->getId()) {
            $parts = $this->emailConfig->parseTemplateIdParts($templateId);
            $templateId = $parts['templateId'];
            $theme = $parts['theme'];
            if ($theme) {
                $template->setForcedTheme($templateId, $theme);
            }
            $template->loadDefault($templateId);
            $template->setData('orig_template_code', $templateId);
        }

        $template->setData(
            'template_variables',
            \Zend_Json::encode($template->getVariablesOptionArray(true))
        );

        try {
            $templateBlock = $this->_view->getLayout()->createBlock(
                \Amasty\PDFCustom\Block\Adminhtml\Template\Edit::class,
                'template_edit',
                [
                    'data' => [
                        'email_template' => $template
                    ]
                ]
            );
            $template->setData('orig_template_currently_used_for', $templateBlock->getCurrentlyUsedForPaths(false));

            $this->getResponse()->representJson(
                \Zend_Json::encode($template->getData())
            );
        } catch (\Exception $e) {
            $this->messageManager->addExceptionMessage($e, $e->getMessage());
        }
    }
}