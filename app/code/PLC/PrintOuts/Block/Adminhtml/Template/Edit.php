<?php

namespace PLC\PrintOuts\Block\Adminhtml\Template;

use Amasty\PDFCustom\Block\Adminhtml\Template\Edit as AmastyEdit;
use Amasty\PDFCustom\Model\ResourceModel\Template\Collection as TemplateCollection;
use Magento\Framework\App\ObjectManager;

class Edit extends AmastyEdit
{
    const AMASTY_OPTION_INDEX = 'Amasty_PDFCustom';

    /**
     * @return mixed
     */
    public function getTemplateOptions()
    {
        $options = parent::getTemplateOptions();
        $templateCollection = ObjectManager::getInstance()->get(TemplateCollection::class);
        try {
            $templates = $templateCollection->toOptionArray();
        } catch (\Exception $e) {
            $templates = [];
        }
        if (!empty($templates)) {
            foreach ($templates as $template) {
                \array_push($options[self::AMASTY_OPTION_INDEX], [
                    'value' => $template['value'],
                    'label' => $template['label'],
                    'group' => self::AMASTY_OPTION_INDEX
                ]);
            }
        }
        return $options;
    }

    public function getLoadUrl()
    {
        return $this->getUrl('plc_printout/template/defaultTemplate');
    }
}