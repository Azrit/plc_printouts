<?php

namespace PLC\PrintOuts\Plugin\Model\ResourceModel;

use PLC\PrintOuts\Model\Config;
use Magento\Framework\App\RequestInterface;

class TemplateRepository
{
    /** @var Config  */
    protected $config;

    /** @var RequestInterface  */
    protected $request;

    public function __construct(
        Config $config,
        RequestInterface $request
    ) {
        $this->config = $config;
        $this->request = $request;
    }

    public function beforeGetTemplateIdByParams($subject, $templateType, $storeId, $customerGroupId)
    {
        if (!$this->config->isEnabled()) {
            return [$templateType, $storeId, $customerGroupId];
        }
        $type = $this->request->getParam('type', null);
        return [$type ?? $templateType, $storeId, $customerGroupId];
    }
}