<?php

namespace PLC\PrintOuts\Block\Adminhtml\Order;

use Magento\Framework\View\Element\Context;
use Magento\Framework\View\Element\Text\ListText;
use Magento\Backend\Block\Widget\Tab\TabInterface;
use PLC\PrintOuts\Model\Config;

class Tab extends ListText implements TabInterface
{
    /** @var Config  */
    protected $config;

    /**
     * Info constructor.
     * @param Context $context
     * @param Config $config
     * @param array $data
     */
    public function __construct(
        Context $context,
        Config $config,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->config = $config;
    }

    /**
     * @return \Magento\Framework\Phrase
     */
    public function getTabLabel()
    {
        return __('PLC: PrintOuts');
    }

    /**
     * @return \Magento\Framework\Phrase
     */
    public function getTabTitle()
    {
        return __('PLC: PrintOuts');
    }

    /**
     * @return bool
     */
    public function canShowTab()
    {
        if (!$this->config->isEnabled()) {
            return false;
        }
        return true;
    }

    /**
     * @return bool
     */
    public function isHidden()
    {
        return false;
    }
}